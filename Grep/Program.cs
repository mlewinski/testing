﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grep
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Too few arguments! See --help");
                return;
            }
            if (args.Contains("--help")) //check if argument list contains --help option
            {
                Console.WriteLine("Grep help :");
                Console.WriteLine("grep.exe pattern [options] --filelist [files]");
                Console.WriteLine("--invert : invert match ");
                Console.WriteLine("--begins : match only lines beginning with the pattern");
                Console.WriteLine("--case-ignore : ignore letters size");
                Console.WriteLine("--filelist : indicates beginning of file list");
                return;
            }

            string pattern = args[0];
            BitArray options = new BitArray(3);
            if (args.Contains("--invert")) options[0] = true;
            if (args.Contains("--begins")) options[1] = true;
            if (args.Contains("--case-ignore")) options[2] = true;

            int fileListBeggining = Array.IndexOf(args, "--filelist") + 1;

            for (int i = fileListBeggining; i < args.Length; i++)
            {
                int lineNumber = 0;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Parsing file : {0}", args[i]);
                Console.ForegroundColor = ConsoleColor.White;

                try
                {
                    using (StreamReader f = new StreamReader(args[i]))
                    {
                        string line = String.Empty;
                        string originalLine = String.Empty;
                        while ((line = f.ReadLine()) != null)
                        {
                            lineNumber++;
                            originalLine = line;
                            if (options[2] == true)
                            {
                                line = line.ToUpper();
                                pattern = pattern.ToUpper();
                            }
                            if (options[0] == true) // --invert
                            {
                                if (options[1] == true) // --begins
                                {
                                    if (!line.StartsWith(pattern)) PrintMatch(args[i], lineNumber, originalLine);
                                    continue;
                                }
                                if (line.Contains(pattern)) continue;
                                PrintMatch(args[i], lineNumber, originalLine);
                            }
                            else
                            {
                                if (options[1] == true) // --begins
                                {
                                    if (line.StartsWith(pattern)) PrintMatch(args[i], lineNumber, originalLine);
                                }
                                else if (line.Contains(pattern)) PrintMatch(args[i], lineNumber, originalLine);
                            }
                        }
                    }
                }
                catch (IOException ioex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error! \n {0}", ioex.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            Console.ReadLine();
        }

        public static void PrintMatch(string fileName, int lineNumber, string line)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("{0} : {1} : {2}", fileName, lineNumber, line);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
