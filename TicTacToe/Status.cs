﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Status
    {
        public static List<List<char>> map = new List<List<char>>();
        static void Main(string[] agrs)
        {
            int size;
            while (true)
            {
                try
                {
                    Console.WriteLine("Podaj rozmiar planszy");
                    size = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }       
            }
            Init(size);
            char Player1, Player2;

            while (true)
            {
                try
                {
                    Console.WriteLine("Podaj symbol pierwszego gracza");
                    Player1 = Convert.ToChar(Console.ReadLine());
                    break;
                }
                catch (Exception e1)
                {
                    Console.WriteLine(e1.Message);
                    continue;
                }
            }
            while (true)
            {
                try
                {
                    Console.WriteLine("Podaj symbol drugiego gracza");
                    Player2 = Convert.ToChar(Console.ReadLine());
                    break;
                }
                catch (Exception e2)
                {
                    Console.WriteLine(e2.Message);
                    continue;
                }
            }
            int FirstMove;
            Console.WriteLine("wybierz rozpoczynajacego gracza (1 lub 2)");
            FirstMove= Convert.ToInt32(Console.ReadLine());
            FirstMove -= 1;
            int a, b, status;
            int round=0;
            bool win = false;

            for (int i=0; i<size*size; i++)
            {
                do
                {
                    Console.WriteLine("Podaj wspolrzedne a i b");
                    a = Convert.ToInt32(Console.ReadLine());
                    b = Convert.ToInt32(Console.ReadLine());

                    if (i % 2 == FirstMove)
                        status = PlayerMove(Player1, a, b);
                    else
                        status = PlayerMove(Player2, a, b);
                    if (status == -1)
                        Console.WriteLine("Wsolrzedna poza zakresem");
                    if (status == -2)
                        Console.WriteLine("Pole jest zajete");

                    Display();

                } while (status != 1);
                if (CheckWin(Player1))
                {
                    Console.WriteLine("Wygral gracz nr 1");
                    win = true;
                    break;
                }
                if(CheckWin(Player2))
                {
                    Console.WriteLine("Wygral gracz nr 2");
                    win = true;
                    break;
                }
                    
                round++;                
            }
            if (!win)
                Console.WriteLine("Remis");

        }
        /// <summary>
        /// Try to place player's sign
        /// </summary>
        /// <param name="sign">Player's sign</param>
        /// <param name="x">Map - x</param>
        /// <param name="y">Map - y</param>
        /// <returns>-1 coordinates exceed size of map, -2 field is occupied, 1 success</returns>
        public static int PlayerMove(char sign, int x, int y)
        {
            if (x >= map.Count || y>=map.Count)
                return -1;
            if (map[x][y] != ' ')
                return -2;
            map[x][y] = sign;
            return 1;

        }
        public static void Display()
        {
            for(int i=0; i <map.Count; i++)
            {
                for(int j=0; j<map.Count; j++)
                {
                    Console.Write("| {0} |", map[i][j]);
                }
                Console.WriteLine();
            }
        }
        public static void Init(int size)
        {
            for (int i = 0; i < size; i++)
            {
                map.Add(new List<char>());
                for (int j = 0; j <size; j++)
                {
                    map[i].Add(' ');
                }
            }
        }
        public static bool CheckWin(char sign)
        {
            bool win;
            for (int row = 0; row < map.Count; row++)
            {
                win = true;
                for (int col = 0; col < map.Count; col++)
                {
                    if (map[row][col] != sign)
                    {
                        win = false;
                        break;
                    }
                }
                if (win) return true;
            }

            for (int row = 0; row < map.Count; row++)
            {
                win = true;
                for (int col = 0; col < map.Count; col++)
                {
                    if (map[col][row] != sign)
                    {
                        win = false;
                        break;
                    }
                }
                if (win) return true;
            }
            win = true;
            if (map[0][0] != sign) return false;
            for (int pos = 1; pos < map.Count; pos++)
            {
                if (map[pos][pos] != map[pos - 1][pos - 1])
                {
                    win = false;
                    break;
                }
            }
            if(win)
                return true;

            if (map[0][map.Count - 1] != sign) return false;
            for (int pos = map.Count - 2; pos > 0; pos--)
            {
                if (map[map.Count - pos - 1][pos] != map[map.Count - pos][pos + 1]) return false;
            }
            return true;
        }
    }
}
