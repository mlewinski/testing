﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int option = -1;
            double x = 0.0;
            double y = 0.0;
            double wynik = 0.0; 
            while (option != 0)
            {
                while (true)
                {
                    try
                    {
                        Console.WriteLine("Podaj x:");
                        x = Convert.ToDouble(Console.ReadLine());
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }

                }

                while (true)
                {
                    try
                    {
                        Console.WriteLine("Podaj y:");
                        y = Convert.ToDouble(Console.ReadLine());
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                Console.WriteLine("1-dodawanie");
                Console.WriteLine("2-odejmowanie");
                Console.WriteLine("3-mnozenie");
                Console.WriteLine("4-dzielenie");
                Console.WriteLine("0-zamknij program");

                option = Convert.ToInt32(Console.ReadLine());
                switch(option)
                {
                    case 1:
                      wynik = Operations.Suma(x, y);
                        break;
                    case 2:
                        wynik = Operations.Odejmowanie(x, y);
                        break;
                    case 3:
                        wynik = Operations.Mnozenie(x, y);
                        break;
                    case 4:
                        wynik = Operations.Dzielenie(x, y);
                        break;
                }

                if (option != 0) Console.WriteLine("Wynik : {0} (x={1},y={2})", wynik,x,y);
                   

            }
        }
        
    }
   
}
