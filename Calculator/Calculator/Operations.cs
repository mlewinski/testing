﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
  public class Operations
    {
        public static double Suma(double x, double y) => x + y;
        public static double Odejmowanie(double x, double y) => x - y;
        public static double Mnozenie(double x, double y) => x * y;
        public static double Dzielenie(double x, double y)
        {
            if (y == 0)
            {
                Console.WriteLine("Y jest rowne 0");
                return double.NaN;
            }
            else
                return x / y;       
        }
    }
}
