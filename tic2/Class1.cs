﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tictactoe
{
    class game
    {
       
        public void draw_board(char[] b) //method that draws board to play
        {
            Console.Write(b[0] + "  | " + b[1] + " | " + b[2]);
            Console.WriteLine();
            for (int i = 1; i < 12; i++)
                if (i % 4 == 0)
                    Console.Write("+");
                else
                    Console.Write("-");
            Console.WriteLine();
            Console.Write(b[3] + "  | " + b[4] + " | " + b[5]);
            Console.WriteLine();
            for (int i = 1; i < 12; i++)
                if (i % 4 == 0)
                    Console.Write("+");
                else
                    Console.Write("-");
            Console.WriteLine();
            Console.Write(b[6] + "  | " + b[7] + " | " + b[8]);
            Console.WriteLine();
        }
      public void move(ref char[] b, char p )
        {
            Console.WriteLine("Player " + p + " makes move (choose digit from 1 to 9)");
            int n;
            while (true)
            {
                try
                {
                    n = Convert.ToInt16(Console.ReadLine()); // checks if user typed number , if not ,user has to type again
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Use digit from 1 to 9 , not a sign");
                }
            }
            while (true)
            {
               if (n > 9 || n < 1)                      // checks if field number is correct, if not , user has to type it again
                {
                    Console.WriteLine("Value out of range! Try again: "); 
                    n = int.Parse(Console.ReadLine());
                }
                else
                    break;
            }
            if (p == 'O')           // if everything is OK, checks which player is playing and puts sign in chosen field
                b[n - 1] = p;
            else
                b[n - 1] = 'X';
        }
    public void choose_player(ref char p1,ref char p2)
        {
            Console.Write("May the first player choose their sign: ");
            Console.WriteLine();
            while (true)
            {
                p1 = Convert.ToChar(Console.Read());
                if (p1 != 'O' && p1 != 'X')             //checks if user typed O or X
                    Console.WriteLine("Only O and X are acceptable"); //if not, returns information and user has to type again
                else
                    break;
            }
            }
    public bool win_lose(char[] b,char p) //checks if any of the players won
        {
            if (b[0] == b[1] && b[1] == b[2]&& b[0]==p)
            {
                
                    Console.WriteLine("Player  "+ p + " wins."); // if 3 signs in row are the same, and belong to the player
                    return true;                        // then he/she wins
            }
            else if (b[0] == b[4] && b[4] == b[8] && b[0]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                    return true;
            }
            else if (b[0] == b[3] && b[3] == b[6] && b[0]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                    return true;
                        }
            else if (b[1] == b[4] && b[4] == b[7] && b[1]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                    return true;
                
            }
            else if (b[2] == b[5] && b[5] == b[8] && b[2]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                    return true;
                
                
            }
            else if (b[2] == b[4] && b[4] == b[6] && b[2]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                return true;
                
            }
            else if (b[3] == b[4] && b[4] == b[5] && b[3]==p)
            {
                Console.WriteLine("Player " + p + " wins.");
                return true;
                
            }
            else if (b[6] == b[7] && b[7] == b[8] && b[6] == p)
            {
                Console.WriteLine("Player " + p + " wins.");
                return true;
            }
            else
                return false;
        }
        public bool draw(char[] b)
        {
            for (int i = 0; i < 9; i++) // if any of the places on the board is empty, then the game can be continued
                if (b[i] != 'O' && b[i]!='X')        // if there are no empty places and no one has won, game is over .
                    return false;
            return true;
        }
        public void end(char[] b, char p1, char p2)
        {
            if (win_lose(b, p1))                    // if first player won, ends game with information about winning
            {
                Console.WriteLine("Game Over! Player " + p1 + " wins");
                Console.ReadLine();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            } else if (win_lose(b, p2))               // if first player did not win, checks if second player won
            {                                    // if so, ends game with information about winning
                    Console.WriteLine("Game Over! Player " + p2 + " wins");
                    Console.ReadLine();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                } else if (draw(b))                       // if no one yet has won, checks if there is draw,
            {                                       // if so , ends game with information about draw
                Console.WriteLine("Draw!");
                Console.ReadLine();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }
    }
}
