﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tictactoe
{
    class Program
    {
       
        static void Main(string[] args)
        {
            char[] board;
            char player1='a', player2='b';
            board = new char[9];
            game tic = new game();
            tic.draw_board(board);
            Console.WriteLine();
            tic.choose_player(ref player1, ref player2);            
            while (true)
            {
            tic.end(board, player1, player2);
            tic.move(ref board, player1);
            Console.Clear();
            tic.draw_board(board);
            tic.end(board, player1, player2);
            tic.move(ref board, player2);
            Console.Clear();
            tic.draw_board(board);
            tic.end(board, player1, player2);
            }
        }
    }
}