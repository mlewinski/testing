﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Files_and_Directories
{
    class Directories
    {
        public static void Display(DirectoryInfo d, string DirectoryPath)
        {
            try
            {
                Directory.SetCurrentDirectory(DirectoryPath);
                {
                    Console.WriteLine("Name: {0} ", d.Name);
                    Console.WriteLine("Full name: {0} ", d.FullName);
                    Console.WriteLine("Creation time: {0} ", d.CreationTime);
                    Console.WriteLine("Last access time: {0} ", d.LastAccessTime);
                    Console.WriteLine("Last write time: {0} ", d.LastWriteTime);
                    Console.WriteLine("Parent: {0} ", d.Parent);
                    Console.WriteLine("Root: {0} ", d.Root);
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine("Error!\n {0}", ioex.Message);
            }
        }
    }
}
