﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Files_and_Directories
{
    class Files
    {
        static public void Display(FileInfo f, string FilePath)
        {
            try
            {
                using (StreamReader F1 = new StreamReader(FilePath))
                {
                    Console.WriteLine("Name: {0} ", f.Name);
                    Console.WriteLine("Full name: {0} ", f.FullName);
                    Console.WriteLine("Creation time: {0} ", f.CreationTime);
                    Console.WriteLine("Directory: {0} ", f.Directory);
                    Console.WriteLine("Extension: {0} ", f.Extension);
                    Console.WriteLine("The file read only: {0} ", f.IsReadOnly);
                    Console.WriteLine("Last access time: {0} ", f.LastAccessTime);
                    Console.WriteLine("Last write time: {0} ", f.LastWriteTime);
                    Console.WriteLine("Last write time: {0} ", f.LastWriteTime);
                    Console.WriteLine("Length: {0} ", f.Length);
 
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine("Error!\n {0}", ioex.Message);
            }
        }      
    }
}
