﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests1
{
    public class AdvancedOperations
    {
        public double Tangens(double deg)
        {
            if(deg % 90 == 0) throw new InvalidOperationException("Tangens has an asimptote in this argument");
            return Math.Tan(deg);
        }
    }
}
