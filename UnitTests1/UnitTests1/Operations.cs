﻿using System;

namespace UnitTests1
{
    public class Operations
    {
        public double Sum(double a, double b)
        {
            return a + b;
        }

        public double Divide(double a, double b)
        {
            if (b == 0.0) throw new DivideByZeroException();
            return a / b;
        }
    }
}
