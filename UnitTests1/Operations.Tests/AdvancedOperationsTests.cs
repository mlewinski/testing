﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTests1;

namespace Operations.Tests
{
    [TestClass]
    public class AdvancedOperationsTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TangensTest()
        {
            AdvancedOperations adop = new AdvancedOperations();

            adop.Tangens(90);
        }

        [TestMethod]
        public void TangensReturnsProperValues()
        {
            AdvancedOperations adop = new AdvancedOperations();

            Assert.AreEqual(adop.Tangens(10), Math.Tan(10));
        }
    }
}
