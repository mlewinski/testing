﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Operations.Tests
{
    [TestClass]
    public class OperationsTest
    {
        [TestMethod]
        public void SumOfTwoPositiveValuesIsPositiveTest()
        {
            UnitTests1.Operations op = new UnitTests1.Operations();

            double a = 10.0;
            double b = 10.0;
            Assert.IsTrue(op.Sum(a, b) > 0);
        }

        [TestMethod]
        public void SumOfTwoNegativeValuesIsNegativeTest()
        {
            UnitTests1.Operations op = new UnitTests1.Operations();

            double a = -10.0;
            double b = -0.5;
            Assert.IsTrue(op.Sum(a,b) < 0);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void DivisionByZeroThrowsException()
        {
            UnitTests1.Operations op = new UnitTests1.Operations();
            op.Divide(1.0, 0.0);
        }
    }
}
