﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_Carlo
{
    class Program
    {
        static void Main(string[] args)
        {
            int Radius=1;
            Console.WriteLine("Podaj liczbe prob: ");
            long NumberofTries=Convert.ToInt64(Console.ReadLine());
            double Counter = 0;
            double Result;
            Random a = new Random();
            for (long i=1; i<=NumberofTries; i++)
            {
                double x = a.NextDouble();
                double y = a.NextDouble();

                if (x*x+y*y<1)
                {
                    Counter++;
                }
            }
            Result = (Counter/NumberofTries)*4.0;
            Console.WriteLine("Pi: {0}", Result);
            Console.ReadLine();
        }
    }
}
