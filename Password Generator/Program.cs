﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Password_Generator
{
    class Program
    {
        public static List<string> TempList = new List<string>();
        static void Main(string[] args)
        {
            uint NofPassowrds;
            int Length;
            Console.WriteLine("Number of passwords to generate: ");
            NofPassowrds = Convert.ToUInt32(Console.ReadLine());
            Console.WriteLine("Length of password: ");
            Length = Convert.ToInt32(Console.ReadLine());;

            string Character;
            Console.WriteLine("Enter characters: ");
            Character = Console.ReadLine();

            StringBuilder Key = new StringBuilder();
            Random RndIndex = new Random();

            for (int j = 0; j < NofPassowrds; j++)
            {
                for (int i = 0; i < Length; i++)
                {
                    int Index = RndIndex.Next(0, Character.Length);
                    Key.Append(Character[Index]);
                }
                TempList.Insert(j, Key.ToString());
                Console.WriteLine("Password: {0}", TempList.ElementAt(j));
                Key.Remove(0, Length);
            }  
        }
    }
}
